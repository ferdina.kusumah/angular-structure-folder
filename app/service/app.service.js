app.factory('__Http', ['$http', '$q', 'config', function ($http, $q, config) {
    var serviceBase = config.apiUrl;
    var _CT = 'Content-Type',
        _EC = 'enctype',
        _MF = 'multipart/form-data;',
        _APPJSON = 'application/json; charset=utf-8';

    var obj = {
        GET: function (q, hdrs) {
            if (hdrs == undefined) hdrs = {};
            hdrs[_CT] = _APPJSON;
            return $http.get(serviceBase + q, {
                'headers': hdrs
            });
        },

        GETPDF: function (q) {
            return $http.get(serviceBase + q, {
                responseType: 'arraybuffer'
            })
        },

        POST: function (q, object, hdrs) {
            if (hdrs == undefined) hdrs = {};
            hdrs[_CT] = _APPJSON;
            return $http({
                method: 'POST',
                url: serviceBase + q,
                headers: hdrs,
                data: object
            });
        },

        PUT: function (q, object, hdrs) {
            if (hdrs == undefined) hdrs = {};
            hdrs[_CT] = _APPJSON;
            return $http({
                method: 'PUT',
                url: serviceBase + q,
                headers: hdrs,
                data: object
            });
        },


        UPLOAD: function (q, object, hdrs) {
            if (hdrs == undefined) hdrs = {};
            hdrs[_CT] = _APPJSON;
            hdrs[_CT] = _MF;
            return $http({
                method: 'PUT',
                url: serviceBase + q,
                headers: hdrs,
                data: object
            });
        },

        DELETE: function (q, hdrs) {
            if (hdrs == undefined) hdrs = {};
            return $http.delete(serviceBase + q, {
                'headers': hdrs
            });
        },

        uploadFile: function (q, file) {
            var fd = new FormData();
            fd.append('file', file);
            return $http.post(serviceBase + q, fd, {
                transformRequest: angular.identity,
                headers: {
                    'Content-Type': undefined
                }
            });
        }
    };
    return obj;

}])