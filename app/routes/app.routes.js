app.config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {
    var route = [
        ['/', {
            templateUrl: 'app/view/components/home/home.inc.html',
            controller: 'home_controller'
        }],
    ];

    for (var i = 0, l = route.length, r; i < l; i++) {
        r = route[i];
        $routeProvider.when(r[0], r[1]);
    }
    
    $routeProvider.otherwise({
        redirectTo: '/'
    });
}]);